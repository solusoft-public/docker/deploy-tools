$azurePassword = ConvertTo-SecureString $env:ARM_CLIENT_SECRET -AsPlainText -Force
$psCred = New-Object System.Management.Automation.PSCredential($env:ARM_CLIENT_ID, $azurePassword)
Connect-AzAccount -Credential $psCred -Tenant $env:ARM_TENANT_ID -Subscription $env:ARM_SUBSCRIPTION_ID -ServicePrincipal
New-AzSqlServerDnsAlias -ServerName $env:server_name -ResourceGroupName $env:resource_group -DnsAliasName $env:alias_name -ErrorAction SilentlyContinue
Get-AzSqlServerDNSAlias -ServerName $env:server_name -ResourceGroupName $env:resource_group