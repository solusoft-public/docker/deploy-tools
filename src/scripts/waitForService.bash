#!/bin/bash
echo "Arguments: Service Host [$1], Target HTTP code [$2]";
while [[ $(curl -s -o /dev/null -w ''%{http_code}'' $1) != "$2" ]]; 
do 
    echo "waiting for service to start";
    sleep 5;
done
echo "Service started"