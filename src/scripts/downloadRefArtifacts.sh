#=============================================================================================================================================================
# 
#          FILE:  downloadJobArtifacts.sh
# 
#         USAGE:  sh ./downloadRefArtifacts.sh <project_id> <git_ref> <job_name> <gitlab_private_token> <output_file_name>
# 
#   DESCRIPTION:  Descarga los artefactos de un pipeline completado a partir de el identificador de proyecto, la referencia de git
#                 (rama de git o tag) y el nombre del job. 
# 
#        AUTHOR:  Solusoft
#       VERSION:  1.0
#       CREATED:  11/02/2022
#=============================================================================================================================================================

echo "Downloading artifact for project $1, ref $2, job name $3 - https://gitlab.com/api/v4/projects/$1/jobs/artifacts/$2/download?job=$3..."
curl --location --output $5 --header "PRIVATE-TOKEN: $4" "https://gitlab.com/api/v4/projects/$1/jobs/artifacts/$2/download?job=$3"
