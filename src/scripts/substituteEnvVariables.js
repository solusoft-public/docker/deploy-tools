/**
 *=============================================================================================================================================================
 * 
 *          FILE:  substituteEnvVariables.js
 * 
 *         USAGE:  node ./substituteEnvVariables.js <folder>
 * 
 *   DESCRIPTION:  Dado una ruta de un directorio, lee todos los archivos del mismo, incluyendo subcarpetas, en busca de archivos con la extensión ".template". 
 *                 Dentro de estos archivos sustituye cada cadena del tipo "$<variable_entorno>" por el valor de la variable de entorno en el sistema donde 
 *                 se ejecute el script. Por ejemplo si tenemos una variable de entorno "ENVIRONMENT_PREFIX=sb-pre", sustituiría en cada archivo la cadena 
 *                 de texto "$ENVIRONMENT_PREFIX" por la cadena "sb-pre".  
 * 
 *        AUTHOR:  Solusoft
 *       VERSION:  1.0
 *       CREATED:  02/01/2020
 *=============================================================================================================================================================
 */

const { readdirSync, lstatSync, readFileSync, writeFileSync } = require('fs');
const { join } = require('path');

const FILE_SUFFIX = ".template";

/**
 * Lee todos los archivos, con extensión 'fileSuffix' de una carpeta, incluyendo subcarpetas
 * @param {String} folder Ruta del directorio a leer
 * @param {String} fileSuffix Extensión de los archivos a filtra
 * @param {Array<String>} files Array para acumular los archivos encontrados
 */
function readFolder(folder, fileSuffix, files) {
  try {
    console.log(`reading folder [${folder}]`);
    readdirSync(folder)
      .map(f => join(folder, f))
      .filter(f => (lstatSync(f).isFile() && f.endsWith(fileSuffix)) || lstatSync(f).isDirectory())
      .map(f => {
        if (lstatSync(f).isDirectory()) readFolder(f, fileSuffix, files);
        else files.push(f);
      });
  } catch (err) {
    console.error(`there was an error reading folder [${folder}]`, err);
  }
}

/**
 * Sustituye, en el contenido de un archivo, todas las cadenas del tipo $<variable_entorno> por el valor de la variable de entorno
 * @param {String} file Ruta del archivo
 */
function replaceEnvVariables(file) {
  try {
    console.log(`replacing env variables for file [${file}]`);
    const content = readFileSync(file, 'utf8');
    let replaced = content;
    for (const envName in process.env) {
      replaced = replaced.replace(new RegExp(`\\$${envName}`, 'g'), process.env[envName]);
    }
    const newFileName = file.replace(/\.template$/, "");
    console.log(`writing file [${file}] to new path [${newFileName}]`);
    writeFileSync(newFileName, replaced, 'utf8');
  } catch (err) {
    console.error(`there was an error replacing env variables for file [${file}]`, err);
  }
}

function main() {
  const folder = process.argv[2];
  const files = [];
  readFolder(folder, FILE_SUFFIX, files);
  for (const file of files) {
    replaceEnvVariables(file);
  }
}

main();