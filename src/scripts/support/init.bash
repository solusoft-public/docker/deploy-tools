#!/bin/bash

function try()
{
    [[ $- = *e* ]]; SAVED_OPT_E=$?
    set +e
}

function throw()
{
    exit $1
}

function catch()
{
    export ex_code=$?
    (( $SAVED_OPT_E )) && set +e
    return $ex_code
}

function throwErrors()
{
    set -e
}

function moveIfExists() {
  if [ -s $1 ]; then
    mv $1 $2
  fi
}
function ignoreErrors()
{
    set +e
}

function echoError() {
    echo "There was an error installing $1 version"
    echo "Stopping process"
}

function installTerraform()
{
  try
  (        
    wget -O /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip https://releases.hashicorp.com/terraform/${TERRAFORM_VERSION}/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
    unzip /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip -d /usr/bin
    rm /tmp/terraform_${TERRAFORM_VERSION}_linux_amd64.zip
    echo "Terraform Installed"
    terraform --version
  )
  catch || {
    echoError "Terraform"
    throw $ex_code
  }
}

function installKubectl()
{
  try
  (        
    KUBECTL_REPOSITORY="https://storage.googleapis.com/kubernetes-release/release"
    wget -O /tmp/kubectl ${KUBECTL_REPOSITORY}/v${KUBECTL_VERSION}/bin/linux/amd64/kubectl
    moveIfExists /tmp/kubectl /usr/bin
    chmod +x /usr/bin/kubectl
    echo "Kubectl Installed"
    kubectl
  )
  catch || {
    echoError "Kubectl"
    throw $ex_code
  }
}

function installHelm()
{
  try
  (        
    HELM_REPOSITORY="https://get.helm.sh"
    wget -O /tmp/helm.tar.gz ${HELM_REPOSITORY}/helm-v${HELM_VERSION}-linux-amd64.tar.gz
    tar xzf /tmp/helm.tar.gz -C /tmp
    mv /tmp/linux-amd64/helm /usr/bin
    rm /tmp/helm.tar.gz
    rm -R /tmp/linux-amd64
    chmod +x /usr/bin/helm
    echo "Helm Installed"
    helm
  )
  catch || {
    echoError "Helm"
    throw $ex_code
  }
}

function installNode()
{
  try
  (        
    apk add nodejs=${NODE_VERSION}
    echo "Node Installed"
    node --version
    echo "Installing NPM..."
    apk add --update npm
    echo "NPM Installed"
    npm --version
  )
  catch || {
    echoError "Node Js"
    throw $ex_code
  }
}

function installDependencies()
{
  if [[ -n "${TERRAFORM_VERSION}" ]]; then
    echo "Installing Terraform ${TERRAFORM_VERSION}..."
    installTerraform
  fi
  if [[ -n "${KUBECTL_VERSION}" ]]; then
    echo "Installing Kubectl ${KUBECTL_VERSION}..."
    installKubectl
  fi
  if [[ -n "${HELM_VERSION}" ]]; then
    echo "Installing Helm ${HELM_VERSION}..."
    installHelm
  fi
  if [[ -n "${NODE_VERSION}" ]]; then
    echo "Installing Node Js ${NODE_VERSION}..."
    installNode
  fi
}

function startShell()
{
  exec /bin/bash
}

installDependencies
startShell

