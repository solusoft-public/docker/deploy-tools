#=============================================================================================================================================================
# 
#          FILE:  downloadJobArtifacts.sh
# 
#         USAGE:  sh ./downloadJobArtifacts.sh <project_id> <pipeline_id> <job_name> <gitlab_private_token> <output_file_name>
# 
#   DESCRIPTION:  Descarga los artefactos de un job completado a partir de el identificador de proyecto, el identificador de pipeline
#                 y el nombre del job. 
# 
#        AUTHOR:  Solusoft
#       VERSION:  1.0
#       CREATED:  11/02/2022
#=============================================================================================================================================================

# Retrieving the job id of the pipeline with status "success" and with artifact
JOB_ID=`curl -sS --header "PRIVATE-TOKEN: $4" "https://gitlab.com/api/v4/projects/$1/pipelines/$2/jobs" | jq --arg JOB_NAME "$3" '.[] | select(.name == $JOB_NAME and .status == "success" and .artifacts_file != null) | .id'`
if [ -z "$JOB_ID" ]; then
  echo "Required asset artifact not found in pipeline #$2"
fi
echo "Downloading artifact for project $1, pipeline $2, job ${JOB_ID} - https://gitlab.com/api/v4/projects/$1/jobs/${JOB_ID}/artifacts..."
curl --location --output $5 --header "PRIVATE-TOKEN: $4" "https://gitlab.com/api/v4/projects/$1/jobs/${JOB_ID}/artifacts"
