# Power Shell base image
FROM mcr.microsoft.com/azure-powershell:7.2.0-alpine-3.13

LABEL maintainer="Solusoft" \
      es.solusoft.name="deploy-tools" \
      es.solusoft.version="1.0.0" \
      es.solusoft.license="MIT"

# Common
RUN apk update
RUN apk add curl jq bash ca-certificates git openssl unzip wget gettext libintl

# Azure CLI
RUN apk add py3-pip
RUN apk add gcc musl-dev python3-dev libffi-dev openssl-dev cargo make
RUN pip install --upgrade pip
RUN pip install azure-cli

# Main
COPY src /src
WORKDIR /src

# Permissions
RUN chmod +x scripts/support/init.bash
RUN chmod +x scripts/downloadJobArtifacts.sh
RUN chmod +x scripts/downloadRefArtifacts.sh
RUN chmod +x scripts/newSQLServerDNSAlias.ps1
RUN chmod +x scripts/substituteEnvVariables.js
RUN chmod +x scripts/waitForService.bash

ENTRYPOINT [ "bash", "-c", "scripts/support/init.bash" ]
CMD [ "pwsh" ]
