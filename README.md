Deploy tools
==================

Imagen basada en la imagen mcr.microsoft.com/azure-cli:2.31.0 con las siguientes herramientas de despliegue:

- Azure CLI: "2.31.0"
- Powershell: "7.2.1"

También incluye scripts para, en Runtime, instalar las siguientes herramientas:
- Terraform
- Kubectl
- Helm
- NodeJs

Además incluye los siguientes scrips:

- substituteEnvVariables.js: Dado una ruta de un directorio, lee todos los archivos del mismo, incluyendo subcarpetas, en busca de archivos con la extensión ".template". Dentro de estos archivos sustituye cada cadena del tipo "$<variable_entorno>" por el valor de la variable de entorno en el sistema donde se ejecute el script. Por ejemplo si tenemos una variable de entorno  "ENVIRONMENT_PREFIX=sb-pre", sustituiría en cada archivo la cadena de texto "$ENVIRONMENT_PREFIX" por la cadena "sb-pre".
- newSQLServerDNSAlias.ps1: crea un alias de base de datos de Azure utilizando el comando New-AzSqlServerDnsAlias de Powershell.
- waitForService.bash: ejecuta un GET HTTP sobre una URL especificada, en espera de un código HTTP especificado, cada cierto tiempo, en bucle.
  
## Configuración

Las herramientas opcionales se instalan utilizando las siguientes variables de entorno:
- TERRAFORM_VERSION: especifica la versión de Terraform a instalar. Si no se especifica esta variable no se instalará ninguna. Por defecto esta variable no tiene valor. Testado con la versión 1.1.2.  
- KUBECTL_VERSION: especifica la versión de Kubectl a instalar. Si no se especifica esta variable no se instalará ninguna. Por defecto esta variable no tiene valor. Testado con la versión 1.23.1.  
- HELM_VERSION: especifica la versión de Helm a instalar. Si no se especifica esta variable no se instalará ninguna. Por defecto esta variable no tiene valor. Testado con la versión 3.7.2.  
- NODE_VERSION: especifica la versión de Node a instalar. Si no se especifica esta variable no se instalará ninguna. Por defecto esta variable no tiene valor. Testado con la versión 14.18.1-r0.  

## Ejecución

La imagen está preparada para funcionar por defecto en Gitlab CI ya que al terminar el script de instalación de dependencias lanza una shell preparada para ejecutar los scripts de Gitlab CI.

## Contributing & License

solusoft  

Todos los derechos reservados. Consultar [www.solusoft.es](http://www.solusoft.es/contactar.aspx)  

