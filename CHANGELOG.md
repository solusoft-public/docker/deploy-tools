# CHANGELOG

All notable changes to this project will be documented in this file.
The format is based on [Keep a Changelog](http://keepachangelog.com/). 
  
## [1.0.0] - 2022-02-28  
  
### Changed  
  
* Utilizada imagen de powershell como nueva imagen base.  
  
## [0.2.1] - 2022-02-22  
  
### Fixed  
  
* Añadidos de nuevo comando RUN pwsh.  
    
## [0.2.0] - 2022-02-11  
  
### Added  
  
* Añadidos scripts para descarga de artefactos  
  
## [0.1.5] - 2022-02-03  
  
### Fixed  
  
* Conversión a formato unix en los scripts  
  
## [0.1.4] - 2022-02-03  
  
### Changed  
  
* Añadida instalación de NPM al instalar Node
  
## [0.1.3] - 2022-02-02  
  
### Changed  
  
* Añadida ejecución de bash shell en script init
  
## [0.1.2] - 2022-02-02  
  
### Changed  
  
* Quitada ejecución de powershell en entrypoint y movida a cmd  
    
## [0.1.1] - 2022-02-02  
  
### Changed  
  
* Modificado CMD por ENTRYPOINT en Dockerfile  
  
## [0.1.0] - 2022-01-10  
  
### Added  
  
* Añadido script para instalar en runtime versiones de Terraform, Kubectl, Helm y NodeJs.  
  
### Changed  
  
* Modificada imagen para tener preinstaladas únicamente las versiones 2.31.0 de Azure CLI y 7.2.1 de Powershell.  
  
## [0.0.9] - 2021-12-20  

### Changed

* Actualizada versión de terraform a 1.1.2  
* Cambiado repositorio de instalación de helm  
* Actualizada versión de helm a 2.17.0  
* Actualizada versión de node a 10.24.1-r0  
  
## [0.0.8] - 2020-06-11  

### Changed

* Actualizada versión de powershell a 6.2.5  
    
## [0.0.7] - 2020-05-18  

### Changed

* Actualizada versión de terraform a 0.12.25  
  
## [0.0.6] - 2020-05-15  

### Changed

* Añadido script power shell para crear alias de base de datos  
  
## [0.0.5] - 2020-05-15  

### Changed

* Instalado Power Shell   
  
## [0.0.4] - 2020-05-13  

### Changed

* Actualizado kubectl a v1.18.0  
  
## [0.0.3] - 2020-05-12  

### Changed

* Actualizado README  
  
## [0.0.2] - 2020-05-12  

### Changed

* Actualizado script substituteEnvVariables para funcionar con rutas absolutas  
  
## [0.0.1] - 2020-05-07  

### Changed

* Versión inicial  